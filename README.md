# Brian Griffin's dot files

Just my basic dev environment set up for vim, Ruby.

TODO: Add more aliases, automated set up of homebrew, maybe node/NPM

## Requirements

* Homebrew
* Ruby
* VIM

## Installation

  `git clone git@bitbucket.org:griffinridge/dotfiles.git ~/dotfiles`  
  `cd ~/dotfiles`  
  `sh install.sh`
