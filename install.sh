########## Variables

dotfiles=~/dotfiles                    # dotfiles directory
oldfiles=~/dotfiles_old                # old dotfiles backup directory
filenames="bash_aliases bash_profile vim gitconfig vimrc"        # list of files/folders to symlink in homedir

##########

# create dotfiles_old in homedir
echo "CREATING $oldfiles for backup of any existing dotfiles..."
mkdir -p $oldfiles
echo "...DONE"

# change to the dotfiles directory
echo "CHANGING to the $dotfiles directory..."
cd $dotfiles
echo "...DONE"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks 
for file in $filenames; do
  echo "MOVING any existing dotfiles from ~/ to $oldfiles..."
  mv ~/.$file $oldfiles/
  echo "CREATING symlink to $file in home directory..."
  ln -s $dotfiles/$file ~/.$file
done
echo "...DONE"

# Vim Plugins
echo "CLONING required vim bundles..."
mkdir -p ~/.vim/bundle
cd ~/.vim/bundle
git clone git@github.com:sjl/badwolf.git
git clone git@github.com:scrooloose/nerdtree.git
git clone git@github.com:tomtom/tcomment_vim.git
git clone git@github.com:tpope/vim-endwise.git
git clone git@github.com:tpope/vim-surround.git
git clone git@github.com:ervandew/supertab.git
echo "...DONE"

# RBEnv
echo "INSTALLING rbenv..."
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo "...DONE"

# Update bash
source ~/.bash_profile
